from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import LoginForm, SignupForm
from django.http import HttpResponse


# Create your views here.
def login_view(request):
    if request.method == "GET":
        form = LoginForm()
        context = {"form": form}
        return render(request, "accounts/login.html", context)
    elif request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
            else:
                form.add_error(None, "Invalid username/password.")
        else:
            return HttpResponse("invalid form data")
    else:
        return HttpResponse("Invalid request method")

    return HttpResponse("An error occurred")


def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password != password_confirmation:
                error = "The passwords do not match."
                context = {"form": form, "error": error}
                return render(request, "signup.html", context)
            user = User.objects.create_user(
                username=username, password=password
            )
            user = authenticate(request, username=username, password=password)
            login(request, user)
            return redirect("list_projects")
    else:
        form = SignupForm()

    context = {"form": form}
    return render(request, "accounts/signup.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")
