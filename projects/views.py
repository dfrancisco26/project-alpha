from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def list_projects(request):
    message = "You are not assigned to any projects"
    projects_list = Project.objects.filter(owner=request.user)
    context = {"projects_list": projects_list, "message": message}
    return render(request, "projects/list.html", context)


def redirect_view(request):
    response = redirect("/projects/")
    return response


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/project_details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/projects/")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
